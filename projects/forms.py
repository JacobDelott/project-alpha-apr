from django import forms
from projects.models import Project, ProjectCreateView


class ProjectForm(forms.ModelForm):
    class Meta:
        model = Project
        fields = (
            "name",
            "description",
            "owner",
        )


class ProjectCreateView(forms.ModelForm):
    class Meta:
        model = ProjectCreateView
        fields = ("name", "description", "owner")


class TaskForm(forms.ModelForm):
    class Meta:
        Model = Project
        fields = ("name",)
