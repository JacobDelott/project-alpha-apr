from django.urls import path
from projects.views import project_list, create_project, show_project


urlpatterns = [
    path("", project_list, name="list_projects"),
    path("create/", create_project, name="create_project"),
    path("<int:id>/", show_project, name="show_project"),
    # path("tasks/create/", create_task, name="create_task"),
    # other URL patterns...
]


# from django.urls import path
# from projects.views import list_projects
# from django.shortcuts import redirect
# from projects.views import

# urlpatterns = [
#     path("", list_projects, name="list_projects"),

# ]
